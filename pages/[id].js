// pages/[id].js
import Head from 'next/head';
import { Inter } from 'next/font/google';
import styles from '@/styles/Home.module.css';

const inter = Inter({ subsets: ['latin'] });

export default function Article({ article }) {
    console.log(article);
  return (
    <>
      <Head>
        <title>{article.name}</title>
        <meta name="description" content={article.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={`${styles.main} ${inter.className}`}>
        <h1>{article.name}</h1>
        <p>{article.desc}</p>
        <img src={article.image.url}></img>
      </main>
    </>
  );
}

export async function getServerSideProps({ params }) {
  const { id } = params;

  // Fetch data for the specific article using the provided ID
  const res = await fetch(`https://xuot-lbtw-dqoj.f2.xano.io/api:tspyhJe8/property/${id}`);
  const article = await res.json();

  return {
    props: {
      article,
    },
  };
}